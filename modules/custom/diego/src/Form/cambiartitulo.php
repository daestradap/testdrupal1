<?php

namespace Drupal\diego\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements an example form.
 */
class cambiartitulo extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'Cambiartitulo';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['info'] = array(
      '#type' => 'fieldset',
      '#title' => 'Informacion del articulo',
    );
      $form['info']['id'] = array(
        '#type' => 'textfield',
        '#title' => 'ID Articulo',
      );
      $form['info']['title'] = array(
        '#type' => 'textfield',
        '#title' => 'Titulo a cambiar',
      );
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!is_int(intval($form_state->getValue('id'))) ) {
      $form_state->setErrorByName('id', $this->t('Debe ser un numero valido.'));
    }
    $nodo = node_load(intval($form_state->getValue('id')));
    if (is_null($nodo)) {
      $form_state->setErrorByName('id', $this->t('Debe ser un numero valido para un Articulo.'));
    }
    if (empty($form_state->getValue('title')) ) {
      $form_state->setErrorByName('title', $this->t('No puede ser vacio.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $nodo = node_load(intval($form_state->getValue('id')));
    if(!is_null($nodo) && $nodo->bundle() === "article"){
      $nodo->get('title')->value = $form_state->getValue('title');
      $nodo->save();
      $this->messenger()->addStatus($this->t('El nuevo titulo del articulo es: @title', ['@title' => $form_state->getValue('title')]));
    }
  }

}
